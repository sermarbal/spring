<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Welcome to Spitter!</h1>
        <a href="<c:url value='/spittles?count=10' />">Spittles</a> |
        <a href="<c:url value="/spittles/spittle" />">Spittle</a> |
        <a href="<c:url value="/spitter/register" />">Register</a> 
    </body>
</html>
