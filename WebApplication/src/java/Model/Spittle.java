package Model;

import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Spittle {
    
    @NotNull
    private int id;
    
    @NotNull
    @Size(min=10, max=100)
    private String message;
    
    private Date time;
    
    @NotNull
    private Float latitude;
    
    @NotNull
    private Float longitude;

    public Spittle(int id, String message, Date time, Float latitude, Float longitude){
        this.id=id;
        this.message=message;
        this.time=time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }
    
    
    
}
