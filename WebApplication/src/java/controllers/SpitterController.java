package controllers;

import Model.Spitter;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller // Declarado para ser controlador
@RequestMapping("/spitter")
public class SpitterController {
    
    Spitter spitter;
    
    @RequestMapping(value="/register", method=GET)
    public String showRegistrationForm(){
        
        return "registerForm";
        
    }

    @RequestMapping(value="/register", method=POST)
    public String processRegistration( Spitter spitter){
        
        this.spitter = spitter;
        
        return "redirect:/spitter/" + spitter.getUsername();
        
    }
    
//    @RequestMapping(value="/register", method=POST)
//    public String processRegistration(@Valid Spitter spitter, Errors errors){
//        
//        if(errors.hasErrors()){
//            return "registerForm";
//        }
//        
//        this.spitter = spitter;
//        return "redirect:/spitter/" + spitter.getUsername();
//        
//    }
    
    @RequestMapping(value="/{username}", method=GET)
    public String showSpitterProfile(@PathVariable String username, Model model){
        
        Spitter spitter = this.spitter;
        model.addAttribute(spitter);
        
        return "profile";
    }
    
}
