//package controllers;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
// 
//@Controller
//@RequestMapping("/helloworld.htm")
//public class HelloWorldController {
// 
//    @RequestMapping(method = RequestMethod.GET)
//    public String helloWorld(ModelMap modelMap) {
//        System.out.println("on method");
//        modelMap.put("printme", "Hello Spring !!");
//        return "home";
//    }
//}

package controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller // Declarado para ser controlador
@RequestMapping({"/", "/homepage"})
public class HomeController{
    
    @RequestMapping(method = GET) // Procesar solicitudes GET de /
    public String home(){
        return "home"; // el nombre de la vista home
    }
}

