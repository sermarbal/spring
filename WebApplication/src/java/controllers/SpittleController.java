package controllers;

import Model.Spittle;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("/spittles")
public class SpittleController {

    private Spittle spittle;

    @RequestMapping(method = RequestMethod.GET)
    public String spittles() {
        return "spittles";
    }

    @RequestMapping(value = "/spittle", method = RequestMethod.GET)
    public String showSpittleForm() {
        return "spittleForm";
    }

    @RequestMapping(value = "/spittle", method = RequestMethod.POST)
    public String processSpittleForm(Spittle spittle) {
        this.spittle = spittle;
        this.spittle.setTime(new Date());
        return "redirect:/spittles/" + spittle.getId();
    }

    @RequestMapping(value = "/{id}", method = GET)
    public String showSpitle(@PathVariable Long id, Model model) {
        Spittle spittle = this.spittle;
        model.addAttribute(spittle);
        return "spittle";
    }

    @ModelAttribute("spittleList")
    public List<Spittle> populatespittleList() {
        List<Spittle> spittleList;
        spittleList = new ArrayList<Spittle>();
        for (int i = 0; i < 5; i++) {
            spittleList.add(new Spittle(i, "Spittle " + i, new Date(), 100.0f, 200.0f));
        }

        return spittleList;
    }
}

//@Controller // Declarado para ser controlador
//@RequestMapping("/spittles")
//public class SpittleController {
//
//    Spittle spittle;
//    
//    @RequestMapping(method = RequestMethod.GET)
//    public String spittles(Model model, @RequestParam(value="count", defaultValue="20") int count) {
//
//        List<Spittle> spittleList = new ArrayList<Spittle>();
//        
//        for (int i = 0; i < count; i++) {
//            spittleList.add(new Spittle(i, "Spittle " + i, new Date(), 100.0f, 200.0f));
//        }
//        model.addAttribute(spittleList);
//        return "spittles";
//    }
//    
//    @RequestMapping(value="/spittle", method=GET)
//    public String showSpittle(Model model, @RequestParam(value="spittle_id", defaultValue="1") int spittle_id){
//        
//        Spittle spittle = new Spittle(spittle_id, "Spittle "+spittle_id, new Date(), 100.0f, 200.0f);
//        model.addAttribute(spittle);
//        
//        return "spittle";
//    }
//    
//    @RequestMapping(value="/{id}", method=GET)
//    public String showSpittle2(@PathVariable int id, Model model){
//        
//        Spittle spittle = this.spittle;
//        model.addAttribute(spittle);
//        
//        return "spittle";
//    }
//    
//}
