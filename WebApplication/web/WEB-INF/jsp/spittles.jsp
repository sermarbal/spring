<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>All the Spittles!</h1>
        <table border="1">
            <tr>
                <th>id</th>
                <th>message</th>
                <th>time</th>
                <th>latitude</th>
                <th>longitude</th>
            </tr>
            <c:forEach items="${spittleList}" var="spittle" >
                <tr>
                    <td><c:out value="${spittle.id}"/></td>
                    <td><c:out value="${spittle.message}"/></td>
                    <td><c:out value="${spittle.time}"/></td>
                    <td><c:out value="${spittle.latitude}"/></td>
                    <td><c:out value="${spittle.longitude}"/></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
